const config        = require('./config.json');
const http          = require('http');
const fs            = require('fs');
const path          = require('path');
const url           = require('url');
const MongoClient   = require('mongodb').MongoClient;
const MongoError    = require('mongodb').MongoError;
const ObjectID      = require('mongodb').ObjectID;
//const { getMongoClient } = require('./db/index');
let getMongoClient = async()=>{

    return new Promise((resolve, reject)=>{
        const url = config.db.url;
        MongoClient.connect(url, { useNewUrlParser: true }, async(error, client)=>{
            if(error){
                console.log('MongoDB: connection error: ', error);
                reject(new MongoError());
            }else{
                resolve(client);
            }
        });
    });
};

http.createServer(async(request, response)=>{

    const { method, url, headers } = request;
    const db = config.db.db;
    const collection = config.db.collection;

    const client = await getMongoClient();
    const col = await client.db(db).collection(collection);

    if(method==='GET' && url === '/locations'){

        await col.find({}).toArray((error, docs)=>{
            if(error){
                throw new MongoError(error);
            }
            if(docs){
                response.writeHead(200, {
                    'Content-Type': 'application/json'
                });
                response.end(JSON.stringify(docs));
            }
        });
    }
    if(method === 'GET' && url.startsWith('/locations/')) {
        let id = url.split('/')[2];

        await col.find({"_id": ObjectID(id)}).toArray((error, doc)=>{
            if(error){
                throw new MongoError(error);
            }
            if(doc){
                response.writeHead(200, {
                    'Content-Type': 'application/json'
                });
                response.end(JSON.stringify(doc));
            }
        })
    }

    if(method === 'POST' && url.startsWith('/locations')){
        let body = '';
        request.on('data', (chunk)=>{
            body += chunk.toString();
        }).on('end', async()=>{
            body = JSON.parse(body);
            if(body){
                await col.insertOne(body, async(error, result)=>{
                    if(error){
                        throw new MongoError(error);
                    }else{
                        response.writeHead(200, {
                            'Content-Type': 'application/json'
                        });
                        if(result.result.ok === 1) {
                            response.end(JSON.stringify(result));
                        }
                    }
                });
            }
        });
    }else if(method === 'PUT' && url.startsWith('/locations/')){
        let body = '';
        let id = url.split('/')[2];
        request.on('data', (chunk)=>{
            body += chunk.toString();
        }).on('end', async()=>{
            body = JSON.parse(body);

            await col.findOneAndUpdate({ "_id": ObjectID(id) }, { $set: {
                    "Country": body.Country,
                    "City": body.City,
                    "location":{
                        "type": "Point",
                        "coordinates": [ body.location.coordinates[0], body.location.coordinates[1] ]
                    }
                } }, (error, result)=>{
                if(error){
                    throw new Error(MongoError(error));
                }else{
                    if(result.ok === 1){
                        response.writeHead(200, {
                            'Content-Type': 'application/json'
                        });
                        response.end(JSON.stringify(result));
                    }
                }
            });
        });
    }else if(method === 'DELETE' && url.startsWith('/locations/')){

        let id = url.split('/')[2];

        await col.findOneAndDelete({"_id": ObjectID(id)}, (error, result)=>{
            if(error){
                response.writeHead(400, {
                    'Content-Type': 'application/json'
                });
                response.end(JSON.stringify(error));
            }
            if(result.ok === 1){
                response.writeHead(200, {
                    'Content-Type': 'application/json'
                });
                response.end(JSON.stringify(result));
            }
        });
    }

}).listen(3000, ()=>{
    console.log('Server listening on *:3000');
});