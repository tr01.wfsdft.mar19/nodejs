const config = {
    port: 8080,
    perMessageDeflate: {
        zlibDeflateOptions: {
        // See zlib defaults.
        chunkSize: 1024,
        memLevel: 7,
        level: 3
        },
        zlibInflateOptions: {
        chunkSize: 10 * 1024
        },
        // Other options settable:
        clientNoContextTakeover: true, // Defaults to negotiated value.
        serverNoContextTakeover: true, // Defaults to negotiated value.
        serverMaxWindowBits: 10, // Defaults to negotiated value.
        // Below options specified as default values.
        concurrencyLimit: 10, // Limits zlib concurrency for perf.
        threshold: 1024 // Size (in bytes) below which messages
        // should not be compressed.
    }
}

const WebSocket = require('ws'),
    WebSocketServer = WebSocket.Server,
    wss = new WebSocketServer(config);


let wssMessage = {
    type: String,
    nickname: String,
    timeStamp: Date,
    message: String,
    users: []
};
let data;
/*
    Broadcast
*/

wss.broadcast = async(wssMessage)=>{

    wss.clients.forEach(async(client)=>{
        if(client.readyState === WebSocket.OPEN){
            client.send(JSON.stringify(wssMessage));
        }
    });
};

wss.on('connection', async(ws)=>{

    console.log(ws._socket.remoteAddress);

    ws.on('message', async(wsMessage)=>{
        
        try{
            data = JSON.parse(wsMessage);
            
            switch(data.type){

                case 'connection':
                console.log(data);
                wssMessage.type = 'connection';
                wssMessage.nickname = data.nickname;
                wssMessage.timeStamp = new Date().toLocaleTimeString();
                wssMessage.message = null;
                if(!wssMessage.users.includes(data.nickname)){
                    wssMessage.users.push(data.nickname);
                }
                wss.broadcast(wssMessage);
                break;

                case 'message':
                console.log(data);
                wss.broadcast(data);
                break;

                case 'typing':
                console.log(data);
                wss.broadcast(data);
                break;

                case 'doneTyping':
                console.log(data);
                wss.broadcast(data);
                break;

                case 'disconnection':
                console.log(data);
                wss.broadcast(data);
                break;
            }

        }catch(e){
            console.error(e);
        }
    });
});