let message     = document.querySelector('#msg');
let textarea    = document.querySelector('#msgContainer');
let msgArea     = document.querySelector('#msgArea');
let typingArea  = document.querySelector('#typingArea')
let usersList   = document.querySelector('#usersList');

let type;
let nickname    = '';
let msg         = '';
let ws          = null;
let typingTimer = 0;
let doneTypingInterval = 1000;

let wssMessage;
let wsMessage = {
    type: String,
    nickname: String,
    timeStamp: String,
    message: String
};

const setNickName = (nickname)=>{

    this.nickname = prompt('nickname: ', 'unnamed');
    wsMessage.nickname = this.nickname;
    appendUserToList(nickname);
};

const appendMessage = (data)=>{

    let text = document.createTextNode(data);
    let li = document.createElement('li');
    li.appendChild(text);
    msgArea.appendChild(li);
};

const appendUserToList = (users)=>{

    usersList.innerHTML = '';
    try{

        for(let user of users){

            let text = document.createTextNode(user);
            let li = document.createElement('li');
            li.appendChild(text);
            usersList.appendChild(li);
        }

    }catch(e){
        console.error(e);
    }
    
};

const updateScroll = (scrolled)=>{
    if(scrolled){
        msgArea.scrollTop = msgArea.scrollHeight;
        usersList.scrollTop = usersList.scrollHeight;
    }
};

/*
    WebSocket client connection
*/

(async()=>{

    // Set Nickname
    await setNickName();

    ws = new WebSocket('ws://192.168.1.149:8080/');

    /*
        EventListener for messages
    */
    message.focus();
    message.addEventListener('change', (event)=>{
        msg = event.target.value;
        msg = new String(new Date()).split(' ')[4] + ' - ' + this.nickname + ' : ' + msg;
    });

    message.onkeyup = (event)=>{

        if(ws.readyState === WebSocket.OPEN && event.keyCode == 13){

            wsMessage.type = 'message';
            wsMessage.nickname = this.nickname;
            wsMessage.timeStamp = new Date().toLocaleTimeString();
            wsMessage.message = msg;
            ws.send(JSON.stringify(wsMessage));
            message.value = '';
            
        }else{

            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
            wsMessage.type = 'typing';
            wsMessage.nickname = this.nickname;
            wsMessage.timeStamp = new Date().toLocaleTimeString();
            wsMessage.message = this.nickname + ' is typing...';
            ws.send(JSON.stringify(wsMessage));
        }
    };

    function doneTyping(){

        wsMessage.type = 'doneTyping';
        wsMessage.nickname = this.nickname;
        wsMessage.timeStamp = new Date().toLocaleTimeString();
        wsMessage.message = this.nickname + ' stopped typing...';
        ws.send(JSON.stringify(wsMessage));
    }

    ws.onopen = ()=>{
        wsMessage.type = 'connection';
        wsMessage.nickname = this.nickname;
        wsMessage.timeStamp = new Date().toLocaleTimeString();
        wsMessage.message = null;
        ws.send(JSON.stringify(wsMessage));
    };

    ws.onmessage = async(wssMessage)=>{

        
        try{
            wssMessage = JSON.parse(wssMessage.data);
            // console.log(wssMessage);

            switch(wssMessage.type){
                case 'connection':
                    console.log(wssMessage);
                    await appendUserToList(wssMessage.users);
                    updateScroll(true);
                    break;
                case 'message':
                    console.log(wssMessage);
                    await appendMessage(wssMessage.message);
                    updateScroll(true);
                    break;
                case 'typing':
                    console.log(wssMessage);
                    typingArea.innerHTML = wssMessage.nickname + ' is typing...';
                break;
                case 'doneTyping':
                    console.log(wssMessage);
                    setTimeout(()=>{
                        typingArea.innerHTML = '';
                    }, 100);
                break;
                // case 'disconnection':
                //     wssMessage.type = 'disconnection';
                //     wssMessage.nickname = this.nickname;
                //     wssMessage.timeStamp = new Date().toLocaleTimeString();
                //     wssMessage.msg = this.nickname + ' has disconnected';
                //     console.log(wssMessage);
                // break;
            }

            ws.onclose = ()=>{
                // wsMessage.type = 'disconnection';
                // wsMessage.nickname = this.nickname;
                // wsMessage.timeStamp = new Date().toLocaleTimeString();
                // wsMessage.message = this.nickname + ' disconnected from webSocket at: ' + wsMessage.timeStamp;
                // ws.send(JSON.stringify(wsMessage));
            };

        }catch(e){
            console.error(e);
        }
    };
})();