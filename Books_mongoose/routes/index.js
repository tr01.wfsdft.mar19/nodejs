const config        = require('../config');
const express       = require('express');
const router        = express.Router();
const mongoose      = require('mongoose');
let connection      = {};
const { parse }     = require('body-parser');
const Book     = require('../models/BookModel');

/* GET home page. */
router.get('/', function(request, response) {
  response.render('index', { title: 'ToDO List' });
});

router.get('/books', async(request, response)=>{

  mongoose.connect(config.db.url, { useNewUrlParser: true });
  connection = mongoose.connection;
  connection.on('error', (error)=>{
    console.error(error);
  });
  connection.once('open', async()=>{
    // console.log('connection opened');
    Book.find((error, books)=>{
      if(error){
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      if(books){
        return response.status(200).json({ ok: true, data: books, fails:[] });
      }
    });
  });
});

router.get('/books/:id', async(request, response)=>{
  const id = request.params.id;
  mongoose.connect(config.db.url, { useNewUrlParser: true });
  connection = mongoose.connection;
  connection.on('error', (error)=>{
    console.error(error);
  });
  connection.once('open', async()=>{
    // console.log('connection opened');
    Book.findById({"_id": id},(error, book)=>{
      if(error){
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      if(book){
        return response.status(200).json({ ok: true, data:[book], fails:[] });
      }
    });
  });
});

//
router.post('/books', async(request, response)=>{

  if(!request.body.title){
    response.status(400).json({ok: false, data:[], fails:[{error: "no body data in request"}]});
  }else{

    mongoose.connect(config.db.url, { useNewUrlParser: true });
    connection = mongoose.connection;
    connection.on('error', (error)=>{
      console.error(error);
    });
    connection.once('open', async()=>{
      // console.log('connection opened');
      const book = new Book({title: request.body.title});
      book.save((error, book)=>{
        if(error){
          return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
        }
        console.log(book);
        if(book){
          return response.status(200).json({ ok: true, data:[{msg: 'Book successfuly saved.', book: book}], fails:[] });
        }
      });
    });
  }
});

router.put('/books/:id', async(request, response)=>{
  const id = request.params.id;
  if(!id){
    response.status(400).json({ ok: false, data:[], fails:[{error: "No ID provided for the book"}] });
  }else{
    mongoose.connect(config.db.url, { useNewUrlParser: true });
    connection = mongoose.connection;
    connection.on('error', (error)=>{
      console.error(error);
    });
    connection.once('open', async()=>{
      // console.log('connection opened');
      Book.findById( id, (error, book)=>{
        if(book){
          book.title = request.body.title;
          book.save((err, storedBook)=>{
            if(err){
              return response.status(500).json({ ok : false, data:[], fails:[{error: err}] });
            }
            return response.status(200).json({ok: true, data:[{msg: 'Book successfully modified.', book: storedBook}], fails:[] });
          });
        }
      });
    });
  }
});

router.delete('/books/:id', async(request, response)=>{
  const id = request.params.id;
  if(!id){
    response.status(400).json({ ok: false, data:[], fails:[{error: "No ID provided for the book"}] });
  }else{
    mongoose.connect(config.db.url, { useNewUrlParser: true });
    connection = mongoose.connection;
    connection.on('error', (error)=>{
      console.error(error);
    });
    connection.once('open', async()=>{
      console.log('connection opened');
      Book.findByIdAndRemove(id, (error, book)=>{
        if(error){
          console.error(error);
          return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
        }
        if(book){
          console.log(book);
          return response.status(200).json({ ok: true, data:[{msg: 'Book successfully deleted.', book: book}], fails:[] });
        }
      });
    });
  }
});

module.exports = router;
