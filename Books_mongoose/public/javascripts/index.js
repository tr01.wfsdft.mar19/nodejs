let create      = document.querySelector('#create');

create.addEventListener('click', async(e)=>{
  let taskname = e.target.parentNode.childNodes[1].value;
  let date = e.target.parentNode.childNodes[3].value;
  await fetch('http://127.0.0.1:3000/tasks', {
    method: 'POST',
    headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body:JSON.stringify({
      taskname: taskname,
      date: date
    })
  }).then(response => response.json())
    .then(async data=>{
      if(data.ok === true){
        let firstCard = document.body.querySelector('.tasks');
        while(firstCard.firstChild){
          firstCard.removeChild(firstCard.firstChild);
        }
        await fetchTasks();
      }
    })
    .catch(e=>console.error(e));
});

const fetchTasks = async ()=>{

  await fetch('http://127.0.0.1:3000/tasks', {
    method: 'GET',
    headers:{
      'Content-Type': 'application/json'
    }
  }).then(response=>response.json())
    .then(data=>{

      if(data.ok === true){

        let obj = data.data;

        obj.forEach(tasks=>{

          tasks.forEach(task=>{
            let tasks = document.querySelector('.tasks');
            let li = document.createElement('li');
            li.classList = 'list-group-item';
            let spanId = document.createElement('span');
            spanId.id = '_id';
            spanId.innerText = task._id;
            let spanTask = document.createElement('span');
            spanTask.id = 'taskname';
            spanTask.innerText = task.taskname;
            let spanDate = document.createElement('span');
            spanDate.id = 'date';
            spanDate.innerText = task.date;


            let editButton = document.createElement('button');
            editButton.id = 'edit';
            editButton.type = 'button';
            editButton.innerText = 'edit';
            editButton.addEventListener('click', (e)=>{
              editTask(e);
            });
            let deleteButton = document.createElement('button');
            deleteButton.addEventListener('click', (e)=>{
              deleteTask(e);
            });
            deleteButton.id = 'delete';
            deleteButton.type = 'button';
            deleteButton.innerText = 'delete';

            li.appendChild(spanId);
            li.appendChild(spanTask);
            li.appendChild(spanDate);
            li.appendChild(editButton);
            li.appendChild(deleteButton);
            tasks.appendChild(li);
          });

        });
      }
    })
    .catch(e=>console.error(e));
};

const editTask = async(e)=>{

  let id        = e.target.parentNode.childNodes[0].innerText;
  let taskname  = e.target.parentNode.childNodes[1].innerText;
  let date      = e.target.parentNode.childNodes[2].innerText;

  let edit                = document.querySelector('#fetch_edit');
  let _id                 = document.querySelector('#id');
  _id.value               = id;
  let editTaskname        = document.querySelector('#editTaskname');
  editTaskname.value      = taskname;
  let editDate            = document.querySelector('#editDate');
  editDate.value          = date;

  edit.addEventListener('click', async(e)=>{

    let _id = e.target.parentNode.childNodes[1].value;
    let t   = e.target.parentNode.childNodes[3].value;
    let d   = e.target.parentNode.childNodes[5].value;
    await fetch(`http://127.0.0.1:3000/tasks/${_id}`,{
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"_id": _id, "taskname": t, "date": d})
    })
      .then(response=>response.json())
      .then(async data => {
        if(data.ok === true){
          let firstCard = document.body.querySelector('.tasks');
          while(firstCard.firstChild){
            firstCard.removeChild(firstCard.firstChild);
          }
          await fetchTasks();
        }
      })
      .catch(e => console.error(e));
  });
};

const deleteTask = async(e)=>{
  let _id = e.target.parentNode.childNodes[0].innerText;
  await fetch(`http://127.0.0.1:3000/tasks/${_id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then(async data => {
      if(data.ok === true){
        let firstCard = document.body.querySelector('.tasks');
        while(firstCard.firstChild){
          firstCard.removeChild(firstCard.firstChild);
        }
        await fetchTasks();
      }
    })
    .catch(e => console.error(e));
};

(async()=>{
  await fetchTasks();

})();




