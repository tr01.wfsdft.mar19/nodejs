const config        = require('./config');
const http          = require('http');
const express       = require('express');
const bodyParser    = require('body-parser');


const indexRouter   = require('./routes/index');

const index = express();
index.use(bodyParser.json());
index.use(bodyParser.urlencoded({ extended : false }));
//index.use(express.urlencoded({ extended : false }));
index.use(express.static(__dirname + '/public'));
index.use('/', indexRouter);

http.createServer(index).listen(config.http.port, ()=>{
  console.log(`server listening on *:${config.http.port}`);
});
