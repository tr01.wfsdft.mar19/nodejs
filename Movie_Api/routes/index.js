const config        = require('../config');
const router        = require('express').Router();
const MongoClient   = require('mongodb').MongoClient;
const MongoError    = require('mongodb').MongoError;
const ObjectID      = require('mongodb').ObjectID;

const getMongoClient = async()=>{
  return new Promise((resolve, reject)=>{
    const url = config.db.url;
    MongoClient.connect(url, { useNewUrlParser: true }, async(error, client)=>{
      if(error){
        reject(new MongoError(error));
      }
      resolve(client);
    });
  });
};

router.get('/movies', async(request, response, next)=>{

  const client = await getMongoClient();
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.find({}).toArray((error, docs)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data: [docs], fails:[] });
  });
});

router.get('/movies/:id', async(request, response, next)=>{
  let id = request.params.id;
  const client = await getMongoClient();
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.find({"_id": ObjectID(id)}).toArray((error, doc)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data: [doc], fails:[] });
  });
});

router.post('/movies', async(request, response, next)=>{
  const client = await getMongoClient();
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.insertOne({
    titulo: request.body.titulo,
    director: request.body.director,
    año: request.body.año
  }, (error, result)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data:[result], fails:[] });
  })
});

router.put('/movies/:id', async(request, response, next)=>{
  let id = request.params.id;
  const client = await getMongoClient();
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.findOneAndUpdate({"_id": ObjectID(id)},{
    $set: {
      titulo: request.body.titulo,
      director: request.body.director,
      año: request.body.año
    }
  }, (error, result)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data:[result], fails:[] });
  });
});

router.delete('/movies/:id', async(request, response, next)=>{
  let id = request.params.id;
  const client = await getMongoClient();
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.findOneAndDelete({"_id": ObjectID(id)}, (error, result)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data:[result], fails:[] });
  });
});

module.exports = router;
