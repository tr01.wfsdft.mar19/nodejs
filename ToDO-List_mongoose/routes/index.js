const config        = require('../config');
const express       = require('express');
const router        = express.Router();
const mongoose      = require('mongoose');
const Task          = require('../models/TaskModel');
let connection      = {};

/* GET home page. */
router.get('/', function(request, response) {
  response.render('index', { title: 'ToDO List' });
});

router.get('/tasks', async(request, response)=>{
  mongoose.connect(config.db.url, { useNewUrlParser: true });
  connection = mongoose.connection;
  connection.on('error', (error)=>{
    console.error(error);
  });
  connection.once('open', async()=>{
    console.log('connection opened');
    await Task.find((error, tasks)=>{
      if(error){
        console.error(error);
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      response.status(200).json({ ok: true, data:[tasks], fails:[] });
    });
  });
});

router.get('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  mongoose.connect(config.db.url, { useNewUrlParser: true });
  connection = mongoose.connection;
  connection.on('error', (error)=>{
    console.error(error);
  });
  connection.once('open', async()=>{
    console.log('connection opened');
    await Task.findById(id,(error, task)=>{
      if(error){
        console.error(error);
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      response.status(200).json({ ok: true, data:[task], fails:[] });
    });
  });
});

router.post('/tasks', async(request, response)=>{

  if(!request.body.taskname && !request.body.date){
    response.status(400).json({ok: false, data:[], fails:[{error: "no body data in request"}]});
  }else{
    mongoose.connect(config.db.url, { useNewUrlParser: true });
    connection = mongoose.connection;
    connection.on('error', (error)=>{
      console.error(error);
    });
    connection.once('open', async()=>{
      const task = new Task({taskname: request.body.taskname, date: new Date(request.body.date)});
      task.save((error, task)=>{
        if(error){
          console.error(error);
          return response.status(400).json({ ok: false, data:[], fails:[{error: 'Bad request.'}] });
        }
        if(task){
          response.status(200).json({ ok: true, data:[task], fails:[] });
        }
      });
    });
  }
});

router.put('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  if(!id){
    response.status(400).json({ ok: false, data:[], fails:[{error: "No ID provided for the task"}] });
  }else{
    mongoose.connect(config.db.url, { useNewUrlParser: true });
    connection = mongoose.connection;
    connection.on('error', (error)=>{
      console.error(error);
    });
    connection.once('open', async()=>{
      // console.log('connection opened');
      Task.findById( id, (error, task)=>{
        if(task){
          task.taskname = request.body.taskname;
          task.date = new Date(request.body.date);
          task.save((err, storedTask)=>{
            if(err){
              return response.status(500).json({ ok : false, data:[], fails:[{error: err}] });
            }
            response.status(200).json({ok: true, data:[storedTask], fails:[] });
          });
        }
      });
    });
  }
});

router.delete('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  mongoose.connect(config.db.url, { useNewUrlParser: true });
  connection = mongoose.connection;
  connection.on('error', (error)=>{
    console.error(error);
  });
  connection.once('open', async()=>{
    console.log('connection opened');
    Task.deleteOne({"_id": id}, (error, result)=>{
      if(error){
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      if(result){
        return response.status(200).json({ ok: true, data:[result], fails:[] });
      }
    });
  });
});

module.exports = router;
