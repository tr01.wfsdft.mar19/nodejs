const mongoose      = require('mongoose');

const TaskSchema    = new mongoose.Schema({
  taskname: String,
  date: Date
});

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;
