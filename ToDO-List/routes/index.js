const config        = require('../config');
const express       = require('express');
const router        = express.Router();
const MongoClient   = require('mongodb').MongoClient;
const MongoError    = require('mongodb').MongoError;
const ObjectID      = require('mongodb').ObjectID;
const { parse }     = require('body-parser');

const getMongoClient = async()=>{
  return new Promise((resolve, reject)=>{
    const url = config.db.url;
    MongoClient.connect(url, { useNewUrlParser: true }, async(error, client)=>{
      if(error){
        reject(error);
      }
      resolve(client);
    });
  });
};

/* GET home page. */
router.get('/', function(request, response) {
  response.render('index', { title: 'ToDO List' });
});

router.get('/tasks', async(request, response)=>{
  const client = await getMongoClient().catch(e=>{
    console.error(e);
  });
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.find({}).toArray((error, docs)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data: [docs], fails:[] });
  });
});

router.get('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  const client = await getMongoClient().catch(e=>{
    console.error(e);
  });
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.find({"_id": ObjectID(id)}).toArray((error, docs)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data: [docs], fails:[] });
  });
});

router.post('/tasks', async(request, response)=>{

  if(!request.body.taskname && !request.body.date){
    response.status(400).json({ok: false, data:[], fails:[{error: "no body data in request"}]});
  }else{
    const client = await getMongoClient().catch(e=>{
      console.error(e);
    });
    const col = client.db(config.db.db).collection(config.db.collection);
    await col.insertOne({
      taskname: request.body.taskname,
      date: request.body.date || new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear()
    }, (error, result)=>{
      if(error){
        response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      response.status(200).json({ ok: true, data:[result], fails:[] });
    });
  }
});

router.put('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  const client = await getMongoClient().catch(e=>{
    console.error(e);
  });
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.findOneAndUpdate({"_id": ObjectID(id)}, {
    $set: {
      taskname: request.body.taskname,
      date: request.body.date
    }
  }, (error, result)=>{
      if(error){
        response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      response.status(200).json({ ok: true, data:[result], fails:[] });
    }
  )
});

router.delete('/tasks/:id', async(request, response)=>{
  const id = request.params.id;
  client = await getMongoClient().catch(e=>{
    console.error(e);
  });
  const col = client.db(config.db.db).collection(config.db.collection);
  await col.findOneAndDelete({"_id": ObjectID(id)}, (error, result)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data:[result], fails:[] });
  });
});

module.exports = router;
