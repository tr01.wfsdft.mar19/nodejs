const http          = require('http');
const express       = require('express');
const path          = require('path');
const bodyParser    = require('body-parser');
const cors          = require('cors');

const indexRouter   = require('./routes/index');

const index         = express();

// view engine setup
index.set('views', path.join(__dirname, 'views'));
index.set('view engine', 'ejs');

index.use(cors({
  origin: 'http://127.0.0.1:3000'
}));

index.use(bodyParser.json());
index.use(express.json());
index.use(express.urlencoded({ extended: false }));
index.use(express.static(path.join(__dirname, 'public')));

index.use('/', indexRouter);

http.createServer(index).listen(3000);
